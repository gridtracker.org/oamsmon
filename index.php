<?php
//OAMSmon is licensed GPLv3
//koolreport is licensed MIT
session_start();
require_once "koolreport/core/autoload.php";
require_once "oamsReport.php";

if($_SERVER["REQUEST_METHOD"] == "POST"){
	$_SESSION=$_POST;
}
$report = new oamsReport;
$report->run()->render();
?>
