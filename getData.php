<?php
  ini_set("allow_url_fopen", 1);
  require_once "mariadb_connect.php";
  require_once "alert.php";
  //$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),);

  $sql = "";
  $serverID = "1";

  try {
    $serverQuery = $mariadb->prepare("SELECT * FROM servers WHERE serverID > '0'");
    $serverQuery->execute();
    $servers = $serverQuery->fetchAll();
  } catch(PDOException $e){
    echo date("Y-m-d H:i:s") . ": Error: " . $e->getMessage();
  }
  $count =0;
  foreach ($servers as $server) {
    $serverID = $server["serverID"];
    echo date("Y-m-d H:i:s") . ": Retreiving data from " . $server["serverName"] . ".\n";
    $data_path = $server["serverURL"];
    $data=json_decode(file_get_contents($data_path), true);
    try {
      $count = 0;
      $mariadb->beginTransaction();
      $dataDate = gmdate("Y-m-d H:i:s", floor(intval($data["date"]) / 1000));
        $sql="INSERT IGNORE INTO data (serverID, dataDate, messageCount, currentClients, clientSessions, highestSessionID, uniqueUsers, spotters, spotsServed, spotsDropped, sourceGT, sourceL4, chatGT, chatL4, socketError, socketLimitMet, jsonMessages, spamEvents, jsonMessageLimit, qslMatched, qslDropped, oldPorts, newPorts)
        values (
          '" . $serverID . "',
          '" . $dataDate . "',
          '" . $data["messageCount"] . "',
          '" . $data["currentClients"] . "',
          '" . $data["clientSessions"] . "',
          '" . $data["highestSessionID"] . "',
          '" . $data["uniqueUsers"] . "',
          '" . $data["spotters"] . "',
          '" . $data["spotsServed"] . "',
          '" . $data["spotsDropped"] . "',
          '" . $data["sourcePrograms"]["GT"] . "',
          '" . $data["sourcePrograms"]["L4"] . "',
          '" . $data["chatBySource"]["GT"] . "',
          '" . $data["chatBySource"]["L4"] . "',
          '" . $data["errors"]["socketError"] . "',
          '" . $data["errors"]["socketLimitMet"] . "',
          '" . $data["errors"]["jsonMessages"] . "',
          '" . $data["errors"]["spamEvents"] . "',
          '" . $data["errors"]["jsonMessageLimit"] . "',
          '" . $data["qslMatched"] . "',
          '" . $data["qslDropped"] . "',
          '" . $data["oldPorts"] . "',
          '" . $data["newPorts"] . "'
        );";
        $ins = $mariadb->prepare($sql);
        $ins->execute();
        $count = $count + $ins->rowCount();
      $mariadb->commit();
      if($ins->rowCount() > 0){
        checkAlerts($serverID,$dataDate);
      }

      echo date("Y-m-d H:i:s") . ": " . $count . " new records for " . $server["serverName"] . " created successfully.\n";
    } catch(PDOException $e) {
      echo date("Y-m-d H:i:s") . ": Error: " . $e->getMessage() . "\nSQL Statement: ". $sql;
    }
  }

$mariadb=null;

?>
