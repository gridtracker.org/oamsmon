<?php

	function checkAlerts($serverID,$dataDate){
	  require "mariadb_connect.php";
	  $alertMessage = "";
	  try {
			$dataQuery = $mariadb->prepare("SELECT * FROM data WHERE serverID = :serverID AND dataDate = :dataDate");
			$dataQuery->execute(["serverID"=>$serverID,"dataDate"=>$dataDate]);
			$data = $dataQuery->fetch();

			$paramsQuery = $mariadb->prepare("SELECT * FROM chartParams WHERE serverID = :serverID AND NOT dataField = 'chatTotal' AND NOT dataField = 'qslTotal' AND NOT dataField = 'spotsTotal'");
			$paramsQuery->execute(["serverID"=>$serverID]);
			$params = $paramsQuery->fetchAll();

			$serverQuery = $mariadb->prepare("SELECT * FROM servers WHERE serverID = :serverID");
			$serverQuery->execute(["serverID"=>$serverID]);
			$server = $serverQuery->fetch();

			$alertsQuery = $mariadb->prepare("SELECT * FROM alerts WHERE serverID = :serverID");
			$alertsQuery->execute(["serverID"=>$serverID]);
			$alerts = $alertsQuery->fetchAll();
			foreach ($alerts as $alert) {
		  	$adminsQuery = $mariadb->prepare("SELECT * FROM sysadmins WHERE adminID = ?");
		  	$adminsQuery->execute([$alert["adminID"]]);
		  	$admins[] = $adminsQuery->fetch();
			}
			foreach ($params as $field) {
				if ($field["redLimit"] <= $data[$field["dataField"]] && $field["alertRed"]) {
					$alertMessage .= $dataDate . "  Red Alert:     " . $server["serverName"] . " " . $field["dataField"] . " is " . $data[$field["dataField"]] . " and the limit is " . $field["redLimit"] . ".\n";
				} elseif ($field["yelLimit"] <= $data[$field["dataField"]] && $field["alertYel"]) {
					$alertMessage .= $dataDate . "  Yellow Alert:  " . $server["serverName"] . " " . $field["dataField"] . " is " . $data[$field["dataField"]] . " and the limit is " . $field["yelLimit"] . ".\n";
				}
			}
	  } catch(PDOException $e){
			echo date("Y-m-d H:i:s") . ": Error: " . $e->getMessage();
  	}
	  if ($alertMessage != ""){
	  	foreach($admins as $admin) {
				mail($admin["email"],"Alert ". $server["serverName"], $alertMessage);
	    }
	  }
	  return $alertMessage;
	}
?>
