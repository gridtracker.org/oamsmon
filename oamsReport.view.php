<?php
	use \koolreport\KoolReport;
	use \koolreport\widgets\google\LineChart;
	use \koolreport\widgets\google\Gauge;
	use \koolreport\widgets\google\PieChart;

	$serverID = 0;

	function makeGuage($dStore, $sID, $dField) {
		try {
      $dbQuery = $GLOBALS["mariadb"]->prepare("SELECT * FROM chartParams WHERE serverID = :serverID AND dataField = :dataField");
      $dbQuery->execute(['serverID' => $sID, 'dataField' => $dField]);
      $params = $dbQuery->fetch();
      } catch(PDOException $e){
        echo "Error: " . $e->getMessage();
      }
		Gauge::create(array("width"=>"150px","height"=>"150px",
      "dataStore"=>$dStore,
		"columns"=>array($dField=>array("label"=>$params["shortLabel"],"type"=>"number")),
      "options"=>array(
        "redFrom"=>$params["redLimit"],"redTo"=>$params["upperLimit"],
      	"yellowFrom"=>$params["yelLimit"],"yellowTo"=>$params["redLimit"],
      	"greenFrom"=>0,"greenTo"=>$params["yelLimit"],
      	"minorTicks"=>5,"max"=>$params["upperLimit"])
			)
		);
	}

	function makeLineChart($dStore, $sID, $dFieldList, $title, $tInt, $scale, $series) {
		$lines["roundedDate"] = array("type"=>"datetime","format"=>"Y-m-d H:i","displayFormat"=>"H:i \n m-d");
		$i = 0;
		foreach($dFieldList as $dField) {
			try {
        $dbQuery = $GLOBALS["mariadb"]->prepare("SELECT * FROM chartParams WHERE serverID = :serverID AND dataField = :dataField");
        $dbQuery->execute(['serverID' => $sID, 'dataField' => $dField]);
        $params = $dbQuery->fetch();
      } catch(PDOException $e){
      	echo "Error: " . $e->getMessage();
      }
			$lines[$dField] = array("label"=>$params["longLabel"],"type"=>"number");
			$sArray[] = array("targetAxisIndex"=>$i);
			if($i < $series){
				$i = $i + 1;
			}
		}
		LineChart::create(array("width"=>"100%","height"=>"500px",
      "dataStore"=>$dStore,"columns"=>$lines,
      "options"=>array("title"=>"$title","curveType"=>"function",
      	"hAxis"=>array("showTextEvery"=>"$tInt"),
				"vAxis"=>array(
					"scaleType"=>$scale,
					"format"=>"short",
					"viewWindow"=>array("min"=>"0")
				),
				"series"=>$sArray
			)
		));
	}

	function makePieChart($dStore, $sID, $dFieldList, $title) {
		foreach($dFieldList as $dField) {
			try {
				$dbQuery = $GLOBALS["mariadb"]->prepare("SELECT * FROM chartParams WHERE serverID = :serverID and dataField = :dataField");
				$dbQuery->execute(['serverID' => $sID, 'dataField' => $dField]);
				$params = $dbQuery->fetch();
			} catch(PDOException $e){
				echo "Error: " . $e->getMessage();
			}
		}
		$lines[$dField] = array("label"=>$params["shortLabel"],"type"=>"number");
		PieChart::create(array(
			"title"=>"$title","dataStore"=>$dStore,"columns"=>$lines
		));
	}


	switch($GLOBALS["dataWindow"]){
		case 0:
			$timeInt = "2"; break;
		case 1:
			$timeInt = "4"; break;
		case 2:
			$timeInt = "8"; break;
		case 3:
			$timeInt = "16"; break;
		case 4:
			$timeInt = "24"; break;
		case 5:
			$timeInt = "28"; break;
		case 6:
			$timeInt = "32"; break;
		case 7:
			$timeInt = "64"; break;
		case 8:
			$timeInt = "192"; break;
		case 9:
			$timeInt = "384"; break;
		case 10:
			$timeInt = "768"; break;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>OAMS Statistics</title>
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
</head>
<!--body onload="javascript:setTimeout(function(){ location.reload(); },1800000);"-->
<div class="report-content">
	<div class="text-center" style="text-align: center;">
	<img src="img/FAO_logo.png" height="339px" alt="OAMS - Off Air Message System">
	<h4>by NR0Q - v1.21.0101</h4>
	<p class="lead">
		Current OAMS statistics (page refreshes every 30min)
	</p>
	<a href="georeport.php" >Show Current Connected Clients GeoReport</a>
	<form action="index.php" method="post">
		<select id="dataLocation" name="dataLocation">
			<?php
				try {
					$dbQuery = $GLOBALS["mariadb"]->prepare("SELECT * FROM servers");
					$dbQuery->execute();
					$servers = $dbQuery->fetchAll();
				} catch(PDOException $e){
					echo "Error: " . $e->getMessage();
				}
				foreach($servers as $server){
					if ($GLOBALS["dataLocation"] == $server["serverRegion"]){
						echo "<option value=\"" . $server["serverRegion"] . "\" selected>" . $server["serverName"] . "</option>\n";
						$serverID = $server["serverID"];
					} else {
						echo "<option value=\"" . $server["serverRegion"] . "\">" . $server["serverName"] . "</option>\n";
					}
				}
			?>
		</select>
		<select id="dataWindow" name="dataWindow">
			<option value="0"<?php if($GLOBALS["dataWindow"] == "0"){echo ' selected';}?>>12 hrs</option>
			<option value="1"<?php if($GLOBALS["dataWindow"] == "1"){echo ' selected';}?>>24 hrs</option>
			<option value="2"<?php if($GLOBALS["dataWindow"] == "2"){echo ' selected';}?>>2 days</option>
			<option value="3"<?php if($GLOBALS["dataWindow"] == "3"){echo ' selected';}?>>4 days</option>
			<option value="4"<?php if($GLOBALS["dataWindow"] == "4"){echo ' selected';}?>>7 days</option>
			<option value="5"<?php if($GLOBALS["dataWindow"] == "5"){echo ' selected';}?>>10 days</option>
			<option value="6"<?php if($GLOBALS["dataWindow"] == "6"){echo ' selected';}?>>2 weeks</option>
			<option value="7"<?php if($GLOBALS["dataWindow"] == "7"){echo ' selected';}?>>1 month</option>
			<option value="8"<?php if($GLOBALS["dataWindow"] == "8"){echo ' selected';}?>>3 months</option>
			<option value="9"<?php if($GLOBALS["dataWindow"] == "9"){echo ' selected';}?>>6 months</option>
			<option value="10"<?php if($GLOBALS["dataWindow"] == "10"){echo ' selected';}?>>1 year</option>
		</select>
		<input type="submit" value="submit">
	</form>
</div>
<div style="margin-left:auto; margin-right:auto; margin-bottom:50px;">
<?php
echo "\n<center>\n<table style='border: 0px;'>\n<tr>\n<td>\n";
	makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "currentClients");
echo "\n</td>\n<td>\n";
	makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "sourceGT");
echo "\n</td>\n<td>\n";
	makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "sourceL4");
echo "\n</td>\n</tr>\n</table>\n</center>\n";
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["currentClients","sourceGT","sourceL4"], "Connected Clients" , $timeInt, "null", 0);
echo "\n<center>\n<table style='border: 0px;'>\n<tr>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "chatTotal");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "chatGT");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "chatL4");
echo "\n</td>\n</tr>\n</table>\n</center>\n";
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["chatTotal","chatGT","chatL4"], "Chat Messages (by sender)" , $timeInt, "log", 0);
echo "\n<center>\n<table style='border: 0px;'>\n<tr>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "spotters");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "spotsTotal");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "spotsServed");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "spotsDropped");
echo "\n</td>\n</tr>\n</table>\n</center>\n";
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["spotters","spotsTotal","spotsDropped","spotsServed"], "Realtime Spots" , $timeInt, "log", 1);
echo "\n<center>\n<table style='border: 0px;'>\n<tr>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "qslTotal");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "qslMatched");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "qslDropped");
echo "\n</td>\n</tr>\n</table>\n</center>\n";
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["qslTotal","qslDropped","qslMatched"], "Instant QSLs" , $timeInt, "log", 1);
echo "\n<center>\n<table style='border: 0px;'>\n<tr>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "clientSessions");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "messageCount");
echo "\n</td>\n</tr>\n</table>\n</center>\n";
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["clientSessions","messageCount"], "Server Stats" , $timeInt, "log", 1);
echo "\n<center>\n<table style='border: 0px;'>\n<tr>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "highestSessionID");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "uniqueUsers");
echo "\n</td>\n</tr>\n</table>\n</center>\n";
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["uniqueUsers"], "Unique users" , $timeInt, "null", 0);
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["highestSessionID"], "Highest Session ID" , $timeInt, "null", 0);
echo "\n<center>\n<table style='border: 0px;'>\n<tr>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "socketError");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "socketLimitMet");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "jsonMessages");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "jsonMessageLimit");
echo "\n</td>\n<td>\n";
  makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "spamEvents");
echo "\n</td>\n</tr>\n</table>\n</center>\n";
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["socketError", "socketLimitMet", "jsonMessages", "jsonMessageLimit", "spamEvents"], "Errors" , $timeInt, "log", 0);
echo "\n<center>\n<table style='border: 0px;'>\n<tr>\n<td>\n";
	makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "oldPorts");
echo "\n</td>\n<td>\n";
	makeGuage($this->dataStore('oams_stats')->bottom(1), $serverID, "newPorts");
echo "\n</td>\n</tr>\n</table>\n</center>\n";
	makeLineChart($this->dataStore('oams_stats'), $serverID, ["oldPorts","newPorts"], "Old/New Ports", $timeInt, "log", 0);
	makePieChart($this->dataStore('oams_stats')->bottom(1), $serverID, ["oldPorts","newPorts"], "Old/New Ports");
?>
</div>
</div>
</body>
</html>
