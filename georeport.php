<?php
ini_set('max_execution_time', 60);
require "./PHP-websocket-client/websocket_client.php";
require_once "./koolreport/core/autoload.php";

use \koolreport\processes\Custom;
use \koolreport\widgets\google\GeoChart;
?>

<html>
<head>
	<title>Current Clients Connected - OAMS Flight Ops</title>
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
</head>
<body>

<div class="report-container">
<div class="text-center">
<h1>OAMS.space Currently Connected Clients</h1>
<?php
class MyReport extends \koolreport\KoolReport
{
	protected function settings() {

	}

	protected function setup() {

	}

}

$host = "tagloomis.com";
$port = mt_rand(18260,18269);
$myCall = "FLTOPS";
$myUUID = "";
$myGrid = "EM39sj";
$myFreq = 0;
$myMode = "SPARK";
$myBand = "OOB";
$myMsg = true;
$myO = 0;
$where = array();

//echo "Connecting to server: $host:$port <br />\n";

if($sp=websocket_open($host, $port, '', $$errstr, 20, true)) {
	websocket_write($sp,json_encode(array('type'=>'uuid','uuid'=>$myUUID,'call'=>$myCall)));
	$rx = websocket_read($sp,$errstr);
	//echo "Server responded with: '" . $rx . "' <br />\n";
	$me = json_decode($rx,true);
	$myUUID = $me["uuid"];
	websocket_write($sp,json_encode(array('type'=>'status','uuid'=>$myUUID,'call'=>$myCall,'grid'=>$myGrid,'freq'=>$myFreq,'mode'=>$myMode,'band'=>$myBand,'canmsg'=>$myMsg,'o'=>$myO)));
	$rx = websocket_read($sp,$errstr);
	do {
		websocket_write($sp,json_encode(array('type'=>'list','uuid'=>$myUUID)));
		$rx = websocket_read($sp,$errstr);
		//echo "Server responded with: '" . $rx . "'<br />\n";
		$list = json_decode($rx,true);
	} while ($list["type"] != "list");
	$dxccData = json_decode(file_get_contents("mh-root-prefixed.json"),true);
	$dxccName = "";
	foreach($list["data"]["calls"] as $callsign){
		if($callsign != $myCall){
			foreach($dxccData as $entity){
				foreach($entity["prefix"] as $prefix){
					if(strpos($callsign, $prefix) === 0){
						$dxccName = $entity["name"];
						break 2;
					}
				}
			}
		}else {
			$dxccName = "United States";
		}
		if($dxccName == "Hawaii" || $dxccName == "Alsaka"){
			$dxccName = "United States";
		}else if($dxccName == "European Russia" || $dxccName == "Asiatic Russia"){
			$dxccName = "Russia";
		}else if($dxccName == "England" || $dxccName == "Isle of Man" || $dxccName == "Northern Ireland" || $dxccName == "Jersey" || $dxccName == "Scotland" || $dxccName == "Gurnsey" || $dxccName == "Wales"){
			$dxccName = "United Kingdom";
		}
		if(!key_exists($dxccName,$where)){
			$where[$dxccName] = 0;
		}
		$where[$dxccName] = $where[$dxccName] + 1;
	}
	foreach($where as $country => $count){
		//echo "Country: $country  Count: $count <br />\n";
		$data[] = array("Country Name"=>$country,"Value"=>$count);
	}
}else {
	echo "Failed to connect to server <br />\n";
	echo "Server responded with : $errstr <br />\n";
}

$report = new MyReport;
$report->run()->render();
GeoChart::create(array(
	"title"=>"Current Connected Clients",
	"dataSource"=>$data,
	"mapsApiKey"=>"myGoogleKey",
	"columns"=>array(
		"Country Name",
		"Value"=>array(
			"type"=>"number",
			"label"=>"Clients Connected"
		)
	),
	"width"=>"`1200px","height"=>"600px",
	"options"=>array(
		"showTooltip"=>true,
		"showInfoWindow"=>false
	)
));
?>
</div>
</div>
</body>
</html>
