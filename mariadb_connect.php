<?php
  global $dbServerName, $dbUserName, $dbPassWord, $dbName, $mariadb;
  $dbServerName = "flightdata-ext.mchambersradio.com";
  $dbUserName = "flightOpsReadOnly ";
  $dbPassWord = "ICanSeeItClearly";
  $dbName = "flightops";

  try {
    $mariadb = new PDO("mysql:host=$dbServerName;dbname=$dbName", $dbUserName, $dbPassWord);
    // Set the PDO error mode to exception
    $mariadb->setAttribute(PDO::ATTR_TIMEOUT, 5);
    $mariadb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected Successfully";
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }
?>
