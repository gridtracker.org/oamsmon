# Flight Ops

A simple page that monitors the current status of the OAMS network server(s) and now sends alerts to sysadmins when data values exceed set thresholds.

As of 10 May 2021, this project is archived and no longer in development. 
The in use Flight Ops system was replaced with Grafana dashboard due to the migration to Prometheus for tracking of performance metrics. All inquiries should be directed to NR0Q@gridtracker.org. 

- All code not contained in the koolreport library directory is GPLv3
- koolreport is licensed under MIT license
