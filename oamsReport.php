<?php
require_once "/home/FlightOps/koolreport/core/autoload.php";
require_once "mariadb_connect.php";
use \koolreport\processes\Group;
use \koolreport\processes\Sort;
use \koolreport\processes\Limit;
use \koolreport\processes\ColumnMeta;
use \koolreport\processes\Filter;
use \koolreport\processes\Map;
use \koolreport\processes\Custom;
use \koolreport\processes\CalculatedColumn;

global $dataWindow, $dataLocation, $serverID, $serverName;
//initial window and location settings if we haven't already selected something
$dataWindow = "4";
$dataLocation = "network-wide";
$serverID = 0;

function roundTime(\DateTime &$datetime, $precision = 30, $round_lower = false) {
	// function borrowed from
	// https://stackoverflow.com/questions/19814427/php-datetime-round-up-to-nearest-10-minutes
	// 1) Set number of seconds to 0 (by rounding up to the nearest minute if necessary)
	$second = (int) $datetime->format("s");
	if ($second > 30 && $round_lower == false) {
			// Jumps to the next minute
			$datetime->add(new \DateInterval("PT".(60-$second)."S"));
	} elseif ($second > 0) {
			// Back to 0 seconds on current minute
			$datetime->sub(new \DateInterval("PT".$second."S"));
	}
	// 2) Get minute
	$minute = (int) $datetime->format("i");
	// 3) Convert modulo $precision
	$minute = $minute % $precision;
	if ($minute > 0) {
			if($round_lower) {
					$datetime->sub(new \DateInterval("PT".$minute."M"));
			} else {
					// 4) Count minutes to next $precision-multiple minutes
					$diff = $precision - $minute;
					// 5) Add the difference to the original date time
					$datetime->add(new \DateInterval("PT".$diff."M"));
			}
	}
}
class oamsReport extends \koolreport\KoolReport
{
	function settings()	{
		return array(
			"dataSources"=>array(
				"mysql_datasource"=>array(
					"connectionString"=>"mysql:host=" . $GLOBALS["dbServerName"] . ";dbname=" . $GLOBALS["dbName"],
					"username"=>$GLOBALS["dbUserName"],
					"password"=>$GLOBALS["dbPassWord"],
					"charset"=>"utf8"
				),
			)
		);
	}
	public function setup()
	{
		if(isset($_SESSION["dataWindow"])){
			$GLOBALS["dataWindow"] = $_SESSION["dataWindow"];
		}
		if(isset($_SESSION["dataLocation"])){
			$GLOBALS["dataLocation"] = $_SESSION["dataLocation"];
		}
		try {
			$dbQuery = $GLOBALS["mariadb"]->prepare("SELECT * FROM servers WHERE serverRegion = :dataLocation");
			$dbQuery->execute(array("dataLocation"=>$GLOBALS["dataLocation"]));
			$srvrData = $dbQuery->fetch(PDO::FETCH_ASSOC);
			$serverID = $srvrData["serverID"];
			$serverName = $srvrData["serverName"];
		} catch(PDOException $e){
			echo "Error: " . $e->getMessage();
		}
		switch($GLOBALS["dataWindow"]){
			case -1:
				$firstDate = strtotime("-1 week"); break;
			case 0:
				$firstDate = strtotime("-12 hours"); break;
			case 1:
				$firstDate = strtotime("-1 day"); break;
			case 2:
				$firstDate = strtotime("-2 days"); break;
			case 3:
				$firstDate = strtotime("-4 days"); break;
			case 4:
				$firstDate = strtotime("-1 week"); break;
			case 5:
				$firstDate = strtotime("-10 days"); break;
			case 6:
				$firstDate = strtotime("-2 weeks"); break;
			case 7:
				$firstDate = strtotime("-1 month"); break;
			case 8:
				$firstDate = strtotime("-3 months"); break;
			case 9:
				$firstDate = strtotime("-6 months"); break;
			case 10:
				$firstDate = strtotime("-1 year"); break;
		}
		$filterDate = gmdate("Y-m-d H:i:s", floor(intval($firstDate)));
		$sql = "SELECT * FROM data WHERE dataDate >= :filterDate";
		$queryParams = array("filterDate"=>$filterDate);
		if ($serverID > 0){
			$sql = $sql . " AND serverID = :serverID";
			$queryParams = array("filterDate"=>$filterDate, "serverID"=>$serverID);
		}
		$this->src('mysql_datasource')
		->query($sql)
		->params($queryParams)
		->pipe(new CalculatedColumn(array(
			"spotsTotal"=>function($data){
				return array_sum(array($data["spotsServed"],$data["spotsDropped"]));
			},
		)))
		->pipe(new CalculatedColumn(array(
			"chatTotal"=>function($data){
				return array_sum(array($data["chatGT"],$data["chatL4"]));
			},
		)))
		->pipe(new CalculatedColumn(array(
			"qslTotal"=>function($data){
				return array_sum(array($data["qslMatched"],$data["qslDropped"]));
			},
		)))
		->pipe(new CalculatedColumn(array(
			"roundedDate"=>function($data){
				$roundedDate = DateTime::createFromFormat("Y-m-d H:i:s", $data["dataDate"]);
				roundTime($roundedDate, 30, true);
				return date_format($roundedDate, "Y-m-d H:i");
			},
		)))
		->pipe($this->dataStore('oams_stats'));
	}
}
?>
